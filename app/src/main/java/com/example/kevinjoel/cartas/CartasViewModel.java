package com.example.kevinjoel.cartas;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CartasViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDao cartaDao;
    private static final int PAGES = 10;
    private MutableLiveData<Boolean> loading;

    public CartasViewModel (Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cartaDao = appDatabase.getCartaDao();
    }


    public LiveData<List<Carta>> getCartas() {
        return cartaDao.getCartas();
    }

    public void reload(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading(){
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {

            CartaApi api = new CartaApi();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
            String rarity = preferences.getString("rarity", "common");
            String tipusConsulta = preferences.getString("tipus_consulta", "tot");

            ArrayList<Carta> result;

            if (tipusConsulta.equals("tot")){
                result = api.getCartas();
            } else {
                result = api.getCardsByRarity(rarity);
            }

            Log.d("DEBUG", result != null ? result.toString() : null);

            cartaDao.deleteCartas();
            cartaDao.addCartas(result);

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {
            super.onPostExecute(cartas);
            loading.setValue(false);
        }
    }
}
