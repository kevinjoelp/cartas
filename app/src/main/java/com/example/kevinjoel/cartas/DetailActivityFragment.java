package com.example.kevinjoel.cartas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;
    private ImageView ivImage;
    private TextView tvTitle;


    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_detail, container, false);

        Intent intent = getActivity().getIntent();

        if (intent != null) {
            Carta carta = (Carta) intent.getSerializableExtra("cards");

            if (carta != null) {
                updateUi(carta);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Carta>() {
            @Override
            public void onChanged(@Nullable Carta carta) {
                updateUi(carta);
            }
        });

        return view;
    }

    private void updateUi(Carta carta) {
        Log.d("CARD", carta.toString());

        ivImage = view.findViewById(R.id.ivImageDetail);
        tvTitle = view.findViewById(R.id.tvTitleDetail);

        tvTitle.setText(carta.getName());
        Glide.with(getContext()).load(carta.getImageURL()).into(ivImage);
    }
}
