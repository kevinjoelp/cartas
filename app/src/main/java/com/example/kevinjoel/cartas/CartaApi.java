package com.example.kevinjoel.cartas;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CartaApi {

    private final String BASE_URL = "https://api.magicthegathering.io/v1/cards";
    private final int PAGES = 5;


    ArrayList<Carta> getCartas() {


        return doCall("cartas");
    }

    ArrayList<Carta> getCardsByRarity(String rarity) {

        return doCall(rarity);

    }

    private String getUrlPage(String rarity, int pagina) {
        Uri buildUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("rarity", rarity)
                .appendQueryParameter("page", String.valueOf(pagina))
                .build();

            return buildUri.toString();

    }

    private ArrayList<Carta> doCall(String rarity){
        ArrayList<Carta> cartas = new ArrayList<>();

        for (int i = 0; i < PAGES ; i++) {
            try {

                String url = getUrlPage(rarity, i);
                String JsonResponse = HttpUtils.get(url);
                ArrayList<Carta> list = processJson(JsonResponse);
                cartas.addAll(list);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return cartas;
    }

    private ArrayList<Carta> processJson(String jsonResponse) {
        ArrayList<Carta> cartes = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCartes = data.getJSONArray("cards");
            for (int i = 0; i < jsonCartes.length(); i++) {
                JSONObject jsonCarta = jsonCartes.getJSONObject(i);

                Carta carta = new Carta();

                carta.setName(jsonCarta.getString("name"));
                carta.setRarity(jsonCarta.getString("rarity"));
                carta.setImageURL(jsonCarta.getString("imageUrl"));
                carta.setType(jsonCarta.getString("type"));

                cartes.add(carta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cartes;
    }

}
