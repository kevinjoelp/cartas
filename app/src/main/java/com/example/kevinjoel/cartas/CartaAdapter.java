package com.example.kevinjoel.cartas;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class CartaAdapter extends ArrayAdapter<Carta> {


    public CartaAdapter(Context context, int resource, List<Carta> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) { // obtener vista que muestre los datos en la posición especificada en el conjunto de datos.

        Carta carta = getItem(position);
        Log.w("XXXX", carta.toString());

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cartas_row, parent, false);
        }

        // Unim el codi en les Views del Layout
        TextView tvTitle = convertView.findViewById(R.id.tvTitleDetail);
        TextView tvBody = convertView.findViewById(R.id.tvBody);

        ImageView ivImage = convertView.findViewById(R.id.ivImageDetail);
        Glide.with(getContext()).load(carta.getImageURL()).into(ivImage);


        tvTitle.setText(carta.getName());
        tvBody.setText(carta.getType());


        return convertView;
    }
}
