package com.example.kevinjoel.cartas;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Carta> selected = new MutableLiveData<>();

    public void selected(Carta carta) {
        selected.setValue(carta);
    }

    public MutableLiveData<Carta> getSelected() {
        return selected;
    }

}
