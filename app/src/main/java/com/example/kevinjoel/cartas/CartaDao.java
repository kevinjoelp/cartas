package com.example.kevinjoel.cartas;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CartaDao {
    @Query("SELECT * FROM carta")
    LiveData<List<Carta>> getCartas();

    @Insert
    void addCarta(Carta carta);

    @Insert
    void addCartas(List<Carta> cartas);

    @Delete
    void deleCarta(Carta carta);

    @Query("DELETE FROM carta")
    void deleteCartas();

}
