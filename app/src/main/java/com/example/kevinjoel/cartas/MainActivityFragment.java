package com.example.kevinjoel.cartas;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private ArrayList<Carta> items;
    private CartaAdapter adapter;
    private SharedPreferences preferences;
    private CartasViewModel model;
    private ProgressDialog dialog;


    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) { //oncreate guardar y recuperar información

        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // https://developer.android.com/guide/topics/ui/menus?hl=es-419
        inflater.inflate(R.menu.menu_cartas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_Refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView lvCartes = view.findViewById(R.id.lvCartas);

        items = new ArrayList<>();

        //adapter = devuelvo una vista para cada objeto de una collecion y lo coloca en un textView.
        adapter = new CartaAdapter(
                getContext(),
                R.layout.lv_cartas_row,
                items //objetos a representar en el ListView
        );

        lvCartes.setAdapter(adapter);

        lvCartes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Carta carta = (Carta ) parent.getItemAtPosition(position);
                if(!esTablet()){
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("carta", carta);
                    startActivity(intent);
                }
            }
        });

        model = ViewModelProviders.of(this).get(CartasViewModel.class);
        model.getCartas().observe(this, new Observer<List<Carta>>() {
            @Override
            public void onChanged(@Nullable List<Carta> cartas) {
                adapter.clear();
                adapter.addAll(cartas);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if(mostrat){
                    dialog.show();
                } else {
                    dialog.dismiss();
                }
            }
        });

        return view;

    }

    boolean esTablet(){
        return getResources().getBoolean(R.bool.tablet);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void refresh() {
        model.reload();
    }



}

